﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BinarySerialization
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Person person = new Person() { FirstName = "kalpana", LastName = "Gajjela" };
            string filePath = "data.Save";
            DataSrializer dataSerializer = new DataSrializer();
            Person p = null;       //to deserialize binary stream , taken another object as null
            dataSerializer.BinarySerialize(person, filePath);

            p = dataSerializer.BinaryDeSerialize(filePath) as Person;

            Console.WriteLine(p.FirstName);
            Console.WriteLine(p.LastName);
            HelloWorld();   //changed by kalpana
            Console.ReadLine();
        }

        private static void HelloWorld()
        {
            throw new NotImplementedException();
        }

        [Serializable]   //this Attribute allow us to do serialization
        public class Person
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }


        }
        class DataSrializer
        {
            public void BinarySerialize(object data, string filePath)
            {
                FileStream fileStream;
                BinaryFormatter bf = new BinaryFormatter();
                if (File.Exists(filePath)) File.Delete(filePath);
                fileStream = File.Create(filePath);
                bf.Serialize(fileStream, data);
                fileStream.Close();

            }
            public object BinaryDeSerialize(string filePath)
            {
                object obj = null;
                FileStream fileStream;
                BinaryFormatter bf = new BinaryFormatter();
                if (File.Exists(filePath))
                {
                    fileStream = File.OpenRead(filePath);
                    obj = bf.Deserialize(fileStream);
                    fileStream.Close();
                }
                return obj;
            }
        }
    }
}
